# Synopsis

I am trying to get my WACOM tablet working and with kernel 4.2.8-300 and need to patch as advised in the following post:


[http://sourceforge.net/p/linuxwacom/mailman/message/34655830/](http://sourceforge.net/p/linuxwacom/mailman/message/34655830/)

# Action

I will be building and patching a kernel from the "Building a Kernel from the Fedora source tree" method, as listed in the very sparse Fedora Kernel building documentaion.

## Build

I have made the hid-core.patch and kernel.spec files available in this repository so you do not need to edit these files.

#### on with the show:

    sudo dnf install fedpkg install audit-libs-devel binutils-devel bison elfutils-devel flex gcc hmaccalc m4 ncurses-devel newt-devel numactl-devel openssl-devel pciutils-devel 'perl(ExtUtils::Embed)' pesign python-devel xz-devel zlib-devel
    dnf --enablerepo=updates-testing update pesign  # see https://bugzilla.redhat.com/show_bug.cgi?id=1283475
    echo "username" >> /etc/pesign/users            # where username is the user that is building the kernel
    sudo /usr/libexec/pesign/pesign-authorize-users
    mkdir kernel_dev
    cd kernel_dev
    fedpkg clone -a kernel
    cd kernel
    git checkout -b f23 --track origin/f23
    git reset --hard 82ccff23e0c3f0ed009e635d25f32174eac5af0d
    cp ~/hid-core.patch .
    cp ~/kernel.spec .
    fedpkg local

And that indeed builds a brand spanking new and patched 4.2.8-300.wacom_fix.fc23.x86_64 kernel.

Note: Where to get that magic number for the `git reset --hard` command?

You need to look in koji for the specific build, and use the sha1sum hash that version was built from. You can do this using the koji command line client. The client method is below:

```
$ koji buildinfo kernel-4.2.8-300.fc23 | head -n 5
BUILD: kernel-4.2.8-300.fc23 [705797]
State: COMPLETE
Built by: jforbes
Volume: DEFAULT
Task: 12201802 build (f23-candidate, /kernel:82ccff23e0c3f0ed009e635d25f32174eac5af0d)
```

## Install

#### Install the new kernel with the following commands:

    sudo rpm -ivh x86_64/kernel-core-4.2.8-300.wacom_fix.fc23.x86_64.rpm
    sudo rpm -ivh x86_64/kernel-modules-4.2.8-300.wacom_fix.fc23.x86_64.rpm
    sudo rpm -ivh x86_64/kernel-4.2.8-300.wacom_fix.fc23.x86_64.rpm
    
and reboot.    

#### Before building the Wacom input-wacom-0.30.1 release you will need to instal the kernel devel rpm;

    sudo rpm -ivh x86_64/kernel-devel-4.2.8-300.wacom_fix.fc23.x86_64.rpm
    
    
# TL;DR

Copy the RPM's from [here](http://www.quodammodo.co.nz/kernels/) and start from the [Install](#install) section.
